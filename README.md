# MIS-Logging

MIS-Logging is a logging package for keeping logging in MIS products consistent.

## Features

  - Supports Flask logging

## Todos

 - Add default logging class
 - Add Django logging class

License
----

[BSD-3-CLAUSE](LICENSE.MD)